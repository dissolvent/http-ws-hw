const users = [];

export function userLogin(id, username) {
  const user = { id, username, ready: false };
  users.push(user);
  return user;
}

export function findByUsername(username) {
  return users.find(user => user.username === username);
}

export function findUserById(id) {
  return users.find(user => user.id === id);
}

export function userDisconnected(id) {
  const index = users.findIndex(user => user.id === id);
  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
}
