import { findUserById } from './users';

const availableRooms = [];

export function createRoom(roomId) {
  availableRooms.push({
    id: roomId,
    users: [],
    maxUsers: false,
    gameStarted: false
  });
}

export function getRoomById(id) {
  return availableRooms.find(room => room.id === id);
}

export function getActiveRooms() {
  return availableRooms.filter(room => !room.maxUsers && !room.gameStarted);
}

export function addUserToRoom(roomId, userId) {
  const room = getRoomById(roomId);
  const user = findUserById(userId);
  if (room) {
    room.users.push({ ...user, ready: false, progress: 0, finished: false });
    const updated = getRoomById(roomId);
    return updated;
  }
}

export function toggleUserReady(roomId, userId) {
  const room = getRoomById(roomId);
  if (room) {
    const userToUpdate = room.users.find(user => user.id === userId);
    // if user establish new connection while in room
    // he still can click "Ready button"
    if (userToUpdate !== undefined) {
      userToUpdate.ready = !userToUpdate.ready;
      return room;
    }
  }
}

export function resetRoomStatus(roomId) {
  const room = getRoomById(roomId);
  if (room) {
    const { users } = room;
    room.gameStarted = false;
    const updated = users.map(user => ({ ...user, progress: 0, ready: false, finished: false }));
    room.users = updated;
  }
}

export function removeUserFromRoom(roomId, userId) {
  const room = getRoomById(roomId);
  if (room) {
    const index = room.users.findIndex(user => user.id === userId);
    if (index !== -1) {
      return room.users.splice(index, 1)[0];
    }
  }
}

export function deleteRoom(roomId) {
  const index = availableRooms.findIndex(room => room.id === roomId);
  if (index !== -1) {
    return availableRooms.splice(index, 1)[0];
  }
}

export function setRoomFull(roomId, full = true) {
  const room = getRoomById(roomId);
  room.maxUsers = full;
}

export function setStartGame(roomId, start = true) {
  const room = getRoomById(roomId);
  room.gameStarted = start;
}

export function updateGameProgress(userId, roomId, progress) {
  const room = getRoomById(roomId);
  if (room?.users.length) {
    const userToUpdate = room?.users.find(user => user.id === userId);
    userToUpdate.progress = progress;
    return room;
  }
}

export function setUserFinished(roomId, userId) {
  const room = getRoomById(roomId);
  if (room?.users.length) {
    const userToUpdate = room.users.find(user => user.id === userId);
    userToUpdate.finished = true;
    return room;
  }
}
