import { Router } from 'express';
import path from 'path';
import { HTML_FILES_PATH } from '../config';
import { getGameText } from '../socket/game';

const router = Router();

router
  .get('/texts/:id', (req, res) => {
    const text = getGameText(req.params.id);
    res.json(text);
  })
  .get('/', (req, res) => {
    const page = path.join(HTML_FILES_PATH, 'game.html');
    res.sendFile(page);
  });

export default router;
