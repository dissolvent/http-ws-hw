/* eslint-disable */
import { addClass, removeClass } from './helpers/helper.mjs';
import { createRoomCard } from './helpers/cardHelper.mjs';
import { 
  renderRoom,
  clearRoom,
  updateUserlist,
  showButtons,
  showCountdown,
  hideCountdown,
  showCountdownEnd,
  showText,
  addText,
  refreshTimer,
  resetUsersStatus } from './room.mjs';
import { fetchText } from './helpers/fetch.mjs';
/* eslint-enable */

const username = sessionStorage.getItem('username');
const roomsPageElement = document.getElementById('rooms-page');
const gamePageElement = document.getElementById('game-page');
const roomsContainer = document.getElementById('rooms-container');
let gameTextArray;
let currentCharacter = 0;
let correctCharacter = 0;
let currentRoom;

if (!username) {
  window.location.replace('/login');
}

// eslint-disable-next-line
const socket = io('/game', { query: { username } });

const login = () => {
  alert('Username already exists. Please try another name.');
  sessionStorage.clear('username');
  window.location.replace('/login');
};

const resetGame = () => {
  currentCharacter = 0;
  correctCharacter = 0;
  gameTextArray = [];
};

const updateRooms = rooms => {
  const allRooms = rooms.map(room => createRoomCard(socket, room));
  roomsContainer.innerHTML = '';
  roomsContainer.append(...allRooms);
};

const createNewRoom = socket => {
  const name = prompt('Enter room name');
  if (!name) {
    return;
  }
  socket.emit('CREATE_NEW_ROOM', name);
};

const joinRoomDone = (socket, room) => {
  currentRoom = room;
  renderRoom(socket, room);
  removeClass(gamePageElement, 'display-none');
  addClass(roomsPageElement, 'display-none');
};

const leaveRoomDone = () => {
  currentRoom = null;
  removeClass(roomsPageElement, 'display-none');
  addClass(gamePageElement, 'display-none');
  clearRoom();
};

const updateRoom = (socket, room) => {
  updateUserlist(room.users);
};

const startCountdown = async ({ id: textId }) => {
  showButtons(false);
  showCountdown();
  const text = await fetchText(textId);
  addText(text);
};

const trackCharacter = event => {
  if (event.location !== 0) {
    return;
  }
  const currentSpan = gameTextArray[currentCharacter];

  if (event.key === currentSpan.innerText) {
    addClass(currentSpan, 'correct');
    correctCharacter += 1;
  }
  removeClass(currentSpan, 'current-character');
  socket.emit('GAME_PROGRESS',
    { percent: correctCharacter / gameTextArray.length,
      roomId: currentRoom.id,
      correctCharacter
    });

  const nextCharacter = currentCharacter + 1;
  const nextSpan = gameTextArray[nextCharacter];
  if (!nextSpan) {
    socket.emit('USER_FINISHED', currentRoom.id);
    document.removeEventListener('keydown', trackCharacter);
  } else {
    addClass(nextSpan, 'current-character');
    currentCharacter = nextCharacter;
  }
};

const startGame = () => {
  resetGame();
  hideCountdown();
  gameTextArray = document.querySelectorAll('#game-text span');
  const currentSpan = gameTextArray[currentCharacter];
  addClass(currentSpan, 'current-character');
  document.addEventListener('keydown', trackCharacter);
  showText();
  showCountdownEnd(true);
};

const showLeaderBoard = users => {
  const leaders = users.map((user, index) => `#${index + 1} ${user.username}\n`);
  alert(leaders);
};

const stopGame = users => {
  showLeaderBoard(users);
  document.removeEventListener('keydown', trackCharacter);
  showButtons();
  showText(false);
  showCountdownEnd(false);
  resetUsersStatus();
};

const createRoomBtn = document.getElementById('create-new-room');
createRoomBtn.addEventListener('click', () => createNewRoom(socket));

socket.on('USER_EXISTS', login);
socket.on('ROOM_EXISTS', () => alert('Room already exists'));
socket.on('UPDATE_ROOMS', updateRooms);
socket.on('JOIN_ROOM_DONE', room => joinRoomDone(socket, room));
socket.on('LEAVE_ROOM_DONE', leaveRoomDone);
socket.on('UPDATE_ROOM', room => updateRoom(socket, room));
socket.on('GAME_COUNTDOWN_START', gameData => startCountdown(gameData));
socket.on('TIME_UNTIL_START', time => {
  refreshTimer('countdown-start', time);
  if (time === 0) {
    startGame();
  }
});
socket.on('TIME_UNTIL_END', time => {
  refreshTimer('countdown-end', time);
});
socket.on('GAME_ENDED', users => stopGame(users));
