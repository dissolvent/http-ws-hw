/* eslint-disable */
import { createElement, addClass, removeClass } from './helpers/helper.mjs';
/* eslint-enable */

const userCard = user => {
  const currentUser = window.sessionStorage.username === user.username;
  const card = createElement({
    tagName: 'div',
    className: 'no-select'
  });

  const attributes = currentUser ? { id: 'active-user' } : {};
  const className = user.ready ? 'ready-circle active' : 'ready-circle';
  const readyCircle = createElement({
    tagName: 'div',
    className,
    attributes
  });

  const name = createElement({
    tagName: 'span'
  });
  name.innerText = currentUser
    ? `${user.username} (you)`
    : user.username;
  const progressBar = createElement({
    tagName: 'div',
    className: 'progress-bar'
  });
  const progressBarInner = createElement({
    tagName: 'div',
    className: 'progress-bar-inner'
  });
  const progress = user?.progress * 100;
  if (progress >= 100) {
    addClass(progressBarInner, 'finished');
  }
  progressBarInner.style.width = `${progress}%`;
  progressBar.append(progressBarInner);

  [readyCircle, name, progressBar].forEach(it => card.append(it));

  return card;
};

function userReady(socket, roomId) {
  socket.emit('USER_READY', roomId);
}

export const addReadyButton = (socket, roomId, { ready }) => {
  const gameScreen = document.getElementById('game-screen');
  const button = createElement({
    tagName: 'button',
    className: 'ui-button ready-button',
    attributes: { id: 'ready-button' }
  });
  button.innerText = 'Not ready';

  let toggle = ready;
  const readyCircle = document.getElementById('active-user');
  button.addEventListener('click', () => {
    button.innerText = toggle ? 'Not ready' : 'Ready';
    toggle = !toggle;
    if (toggle) {
      addClass(readyCircle, 'active');
    } else {
      removeClass(readyCircle, 'active');
    }
    userReady(socket, roomId);
  });
  gameScreen.append(button);
};

export const addCountdown = (parentElement, id) => {
  const countDown = createElement({
    tagName: 'span',
    className: `${id} display-none`,
    attributes: { id }
  });
  parentElement.append(countDown);
};

export const showCountdown = () => {
  const countDown = document.getElementById('countdown-start');
  removeClass(countDown, 'display-none');
};

export const hideCountdown = () => {
  const countDown = document.getElementById('countdown-start');
  addClass(countDown, 'display-none');
};

export const refreshTimer = (elementId, time) => {
  const timer = document.getElementById(elementId);
  timer.innerText = time;
};

const onLeaveRoom = (socket, id) => {
  socket.emit('LEAVE_ROOM', id);
};

const createRoomHeader = id => {
  const roomName = createElement({
    tagName: 'h2',
    className: 'room-name',
    attributes: { id: 'room-name' }
  });
  roomName.innerText = id;
  return roomName;
};

const createBackButton = () => {
  const button = createElement({
    tagName: 'button',
    className: 'ui-button',
    attributes: { id: 'leave-room' }
  });
  button.innerText = 'Back to the rooms';
  return button;
};

export const renderRoom = (socket, { id, users }) => {
  // const leaveRoomBtn = document.getElementById('leave-room');
  const roomInfo = document.getElementById('room-info');

  const roomName = createRoomHeader(id);
  roomInfo.append(roomName);

  const leaveRoomBtn = createBackButton();
  roomInfo.append(leaveRoomBtn);
  leaveRoomBtn.addEventListener('click', () => onLeaveRoom(socket, id));

  const userslist = users.map(userCard);

  const usersElement = document.getElementById('users');
  usersElement.append(...userslist);

  const user = users.filter(user => user.username === window.sessionStorage.getItem('username'))[0];
  addReadyButton(socket, id, user);

  const gameScreen = document.getElementById('game-screen');
  addCountdown(gameScreen, 'countdown-start');
  addCountdown(gameScreen, 'countdown-end');

  const textField = createElement({
    tagName: 'p',
    className: 'game-text display-none',
    attributes: { id: 'game-text' }
  });
  gameScreen.append(textField);
};

export const updateUserlist = users => {
  const usersElement = document.getElementById('users');
  const userslist = users.map(userCard);
  usersElement.innerHTML = '';
  usersElement.append(...userslist);
};

export function clearGameScreen() {
  const gameScreen = document.getElementById('game-screen');
  gameScreen.innerHTML = '';
}

export function clearRoom() {
  const roomInfo = document.getElementById('room-info');
  roomInfo.innerText = '';

  const usersElement = document.getElementById('users');
  usersElement.innerHTML = '';

  clearGameScreen();
}

export function showButtons(show = true) {
  const leaveRoomBtn = document.getElementById('leave-room');
  const readyButton = document.getElementById('ready-button');
  if (show) {
    removeClass(leaveRoomBtn, 'display-none');
    removeClass(readyButton, 'display-none');
  } else {
    addClass(leaveRoomBtn, 'display-none');
    addClass(readyButton, 'display-none');
  }
}

export function showText(show = true) {
  const textElement = document.getElementById('game-text');
  if (show) {
    removeClass(textElement, 'display-none');
  } else {
    addClass(textElement, 'display-none');
  }
}

export function addText(text) {
  const textElement = document.getElementById('game-text');
  textElement.innerHTML = '';

  text.split('').forEach(character => {
    const charSpan = createElement({ tagName: 'span' });
    charSpan.innerText = character;
    textElement.append(charSpan);
  });
}

export const showCountdownEnd = (show = true) => {
  const countDown = document.getElementById('countdown-end');
  if (show) {
    removeClass(countDown, 'display-none');
  } else {
    addClass(countDown, 'display-none');
  }
};

export const resetUsersStatus = () => {
  const progressBar = document.querySelectorAll('.progress-bar-inner');
  const userReadyCircles = document.querySelectorAll('.ready-circle');
  if (progressBar) {
    // eslint-disable-next-line
    progressBar.forEach(bar => bar.style.width = '0%')
    userReadyCircles.forEach(circle => removeClass(circle, 'active'));
  }
};
